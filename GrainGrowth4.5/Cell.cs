﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace GrainGrowth4._5
{
    [Serializable]
    public class Cell
    {
        public int x;
        public int y;
        public int state;
        public bool isBorder;
        [XmlIgnore]
        public Cell[] myNeighbors;
        [XmlIgnore]
        public Cell[] neighbors;

        public Cell()
        {
            state = 0;
            isBorder = false;
            myNeighbors = new Cell[8];
            neighbors = new Cell[8];
        }
    }
}