﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GrainGrowth4._5
{
    [Serializable]
    public class Automat
    {
        public Cell [][]firstGrid;
        public Cell [][]secondGrid;
        public int gridSize;
        Random random = new Random();
        public static int alfaID = 1;
        public static int betaID = 2;

        public Automat(int gridSize)
        {
            this.gridSize = gridSize;
            createGrid();
        }

        public void createGrid()
        {
            firstGrid = new Cell[this.gridSize][];
            secondGrid = new Cell[this.gridSize][];

            for (int i = 0; i < this.gridSize; i++)
            {
                firstGrid[i] = new Cell[gridSize];
                secondGrid[i] = new Cell[gridSize];

                for (int j = 0; j < this.gridSize; j++)
                {
                    firstGrid[i][j] = new Cell();
                    secondGrid[i][j] = new Cell();
                }
            }

        }
        public void createAlfaCell(int count)
        {
            for (int i = 0; i < count; i++)
            {
                int randX = random.Next(0, this.gridSize);
                int randY = random.Next(0, this.gridSize);

                this.firstGrid[randX][randY].state = alfaID;

                alfaID += 2;
            }
        }
        public void createBetaCell(int count)
        {
            for (int i = 0; i < count; i++)
            {
                int randX = random.Next(0, this.gridSize);
                int randY = random.Next(0, this.gridSize);

                this.firstGrid[randX][randY].state = betaID;

                betaID += 2;
            }
        }

        public int mod(int a, int b)
        {
            if (b < 0)
                return mod(-a, -b);
            int ret = a % b;
            if (ret < 0)
                ret += b;
            return ret;
        }

        public void setMyHexNeighbors()
        {
            for (int i = 0; i < this.gridSize; i++)
                for (int j = 0; j < this.gridSize; j++)
                {

                    int above = mod((i - 1), this.gridSize);
                    int below = mod((i + 1), this.gridSize);
                    int left = mod((j - 1), this.gridSize);
                    int right = mod((j + 1), this.gridSize);

                    firstGrid[i][j].myNeighbors[0] = firstGrid[above][ left];
                    firstGrid[i][j].myNeighbors[1] = firstGrid[below][right];
                    firstGrid[i][j].myNeighbors[2] = firstGrid[above][j];
                    firstGrid[i][j].myNeighbors[3] = firstGrid[i][right];
                    firstGrid[i][j].myNeighbors[4] = firstGrid[i][left];
                    firstGrid[i][j].myNeighbors[5] = firstGrid[below][j];
                    firstGrid[i][j].myNeighbors[6] = firstGrid[above][right];
                    firstGrid[i][j].myNeighbors[7] = firstGrid[below][left];
                }
        }
        public void setHexNeighbors()
        {
            for (int i = 0; i < this.gridSize; i++)
                for (int j = 0; j < this.gridSize; j++)
                {

                    int above = mod((i - 1), this.gridSize);
                    int below = mod((i + 1), this.gridSize);
                    int left = mod((j - 1), this.gridSize);
                    int right = mod((j + 1), this.gridSize);

                    firstGrid[i][j].neighbors[0] = secondGrid[above][left];
                    firstGrid[i][j].neighbors[1] = secondGrid[below][right];
                    firstGrid[i][j].neighbors[2] = secondGrid[above][j];
                    firstGrid[i][j].neighbors[3] = secondGrid[i][right];
                    firstGrid[i][j].neighbors[4] = secondGrid[i][left];
                    firstGrid[i][j].neighbors[5] = secondGrid[below][j];
                    firstGrid[i][j].neighbors[6] = secondGrid[above][right];
                    firstGrid[i][j].neighbors[7] = secondGrid[below][left];
                }
        }
        public void hexGrowth()
        {
            for (int i = 0; i < this.gridSize; i++)
                for (int j = 0; j < this.gridSize; j++)
                {
                    secondGrid[i][j].state = firstGrid[i][j].state;
                }

            for (int i = 0; i < this.gridSize; i++)
            {
                for (int j = 0; j < this.gridSize; j++)
                {
                    if (secondGrid[i][j].state != 0)
                    {
                        int choice = random.Next(1, 3);

                        if (choice == 1)
                        {
                            for (int k = 0; k < 6; ++k)
                            {
                                if (firstGrid[i][j].neighbors[k].state == 0)
                                {
                                    firstGrid[i][j].myNeighbors[k].state = secondGrid[i][j].state;
                                }
                            }
                        }
                        else if (choice == 2)
                        {
                            for (int k = 2; k < 8; ++k)
                            {
                                if (firstGrid[i][j].neighbors[k].state == 0)
                                {
                                    firstGrid[i][j].myNeighbors[k].state = secondGrid[i][j].state;

                                }
                            }
                        }
                    }
                }

            }
        }
        public void setBoundaries()
        {
            for (int i = 0; i < gridSize; i++){
		    for (int j = 0; j < gridSize; j++){
			for (int k = 0; k < 8; ++k){
				if (firstGrid[i][j].myNeighbors[k].state != firstGrid[i][j].state){
                    firstGrid[i][j].isBorder = true;
				}
			}
		}
	}
        }
    }
}