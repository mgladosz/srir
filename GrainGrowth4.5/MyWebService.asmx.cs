﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace GrainGrowth4._5
{
    /// <summary>
    /// Summary description for MyWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MyWebService : System.Web.Services.WebService
    {
        public int gridSize;
        Automat automat;

        [WebMethod]
        public Cell [][] FillMatrix(int[][] matrix, int size, int alfa, int beta, int iteration)
        {
            gridSize = size;
            this.automat = new Automat(gridSize);
            this.automat.createAlfaCell(alfa);
            this.automat.createBetaCell(beta);
            automat.setHexNeighbors();
            automat.setMyHexNeighbors();

            for (int i = 0; i < iteration; i++)
            {
                automat.hexGrowth();
            }

            automat.setBoundaries();

            return automat.firstGrid;

        }
    }
}
