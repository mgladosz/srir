﻿namespace WFConsume
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.runAutomat = new System.Windows.Forms.Button();
            this.PB = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxGS = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAC = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxBC = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxIter = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.PB)).BeginInit();
            this.SuspendLayout();
            // 
            // runAutomat
            // 
            this.runAutomat.Location = new System.Drawing.Point(12, 213);
            this.runAutomat.Name = "runAutomat";
            this.runAutomat.Size = new System.Drawing.Size(62, 23);
            this.runAutomat.TabIndex = 0;
            this.runAutomat.Text = "Run";
            this.runAutomat.UseVisualStyleBackColor = true;
            this.runAutomat.Click += new System.EventHandler(this.runAutomat_Click);
            // 
            // PB
            // 
            this.PB.BackColor = System.Drawing.Color.White;
            this.PB.Location = new System.Drawing.Point(80, 12);
            this.PB.Name = "PB";
            this.PB.Size = new System.Drawing.Size(640, 640);
            this.PB.TabIndex = 1;
            this.PB.TabStop = false;
            this.PB.Paint += new System.Windows.Forms.PaintEventHandler(this.PB_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Grid Size";
            // 
            // textBoxGS
            // 
            this.textBoxGS.Location = new System.Drawing.Point(12, 28);
            this.textBoxGS.Name = "textBoxGS";
            this.textBoxGS.Size = new System.Drawing.Size(62, 20);
            this.textBoxGS.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Alfa Count";
            // 
            // textBoxAC
            // 
            this.textBoxAC.Location = new System.Drawing.Point(12, 67);
            this.textBoxAC.Name = "textBoxAC";
            this.textBoxAC.Size = new System.Drawing.Size(61, 20);
            this.textBoxAC.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Beta Count";
            // 
            // textBoxBC
            // 
            this.textBoxBC.Location = new System.Drawing.Point(12, 106);
            this.textBoxBC.Name = "textBoxBC";
            this.textBoxBC.Size = new System.Drawing.Size(62, 20);
            this.textBoxBC.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Iteration";
            // 
            // textBoxIter
            // 
            this.textBoxIter.Location = new System.Drawing.Point(12, 149);
            this.textBoxIter.Name = "textBoxIter";
            this.textBoxIter.Size = new System.Drawing.Size(62, 20);
            this.textBoxIter.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 660);
            this.Controls.Add(this.textBoxIter);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxBC);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxAC);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxGS);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PB);
            this.Controls.Add(this.runAutomat);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.PB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button runAutomat;
        private System.Windows.Forms.PictureBox PB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxGS;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxAC;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxBC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxIter;
    }
}

