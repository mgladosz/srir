﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFConsume.localhost;
//using WFConsume.WebReference;

namespace WFConsume
{
    public partial class Form1 : Form
    {
        private int size;
        private localhost.Cell [][]cells = null;
        //private WebReference.Cell[][] cells = null;
        public Form1()
        {
            InitializeComponent();
            size = 300;
            textBoxGS.Text = Convert.ToString(size);
            textBoxAC.Text = Convert.ToString(50);
            textBoxBC.Text = Convert.ToString(50);
            textBoxIter.Text = Convert.ToString(30);
            
        }

        private void runAutomat_Click(object sender, EventArgs e)
        {
            size = int.Parse(textBoxGS.Text);

            
            var myMatrix = new int[size][];

            for (int i = 0; i < size; i++)
            {
                myMatrix[i] = new int[size];
                for (int j = 0; j < size; j++)
                    myMatrix[i][j] = 0;
            }

            MyWebService cw = new MyWebService();

            cells = cw.FillMatrix(myMatrix, size, int.Parse(textBoxAC.Text), int.Parse(textBoxBC.Text), int.Parse(textBoxIter.Text));


            this.Refresh();
        }

        private void PB_Paint(object sender, PaintEventArgs e)
        {

            int cellSize = 1;

            if (cells != null)
            {

            Pen p = new Pen(Color.Gray);


            for (int y = 0; y <= size; ++y)
            {
                e.Graphics.DrawLine(p, 0, y * cellSize, size * cellSize, y * cellSize);
            }

            for (int x = 0; x <= size; ++x)
            {
                e.Graphics.DrawLine(p, x * cellSize, 0, x * cellSize, size * cellSize);
            }

            
            
                
            for (int x = 0; x < size; ++x)
                for (int y = 0; y < size; ++y)
                {
                    if (cells[x][y].state % 2 == 1 && cells[x][y].state !=0)
                        e.Graphics.FillRectangle(new System.Drawing.SolidBrush(Color.Cyan), new Rectangle(x * cellSize, y * cellSize, cellSize, cellSize));
                    else if (cells[x][y].state % 2 == 0 && cells[x][y].state != 0)
                        e.Graphics.FillRectangle(new System.Drawing.SolidBrush(Color.Yellow), new Rectangle(x * cellSize, y * cellSize, cellSize, cellSize));

                    if (cells[x][y].isBorder == true)
                    {
                        e.Graphics.FillRectangle(new System.Drawing.SolidBrush(Color.Black), new Rectangle(x * cellSize, y * cellSize, cellSize, cellSize));
                    }
                }
            
            }
            //this.PB.Refresh();

        }
    }
}
